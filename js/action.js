$(document).ready(function() {

    var source = $('#todo-list-item-template').html();
    var todoTemplate = Handlebars.compile(source);

    // Update
    $('#todo-list')
        
        .on('dblclick', '.content', function(e){
            $(this).prop('contenteditable', true).focus();
        })
        .on('blur', '.content', function(e) {
            var isNew = $(this).closest('li').is('.new')
            // create
            if (isNew) {
                var todo = $(this).text();
                todo = todo.trim();
        
                if (todo.length>0) {
                    todo = {
                        is_complete: false,
                        content: todo,
                    };
                    var li = todoTemplate(todo)
                    $(this).closest('li').before(li);
                }
                // $(this) = $(e.currentTarget),
                /// in this case is equal to $('li.new').find('content')
                $(e.currentTarget).empty();
            }
            // update
            else {
                $(this).prop('contenteditable', false)
            }
        })
        // delete
        .on('click', '.delete', function(e) {
            var result = confirm('Do you really want to delete?');
            if (result) {
                $(this).closest('li').remove();
            }
        })
        // complete
        .on('click', '.checkbox', function(e) {
            $(this).closest('li').toggleClass('complete');
        });
    $('#todo-list').find('ul').sortable({
        items: 'li:not(.new)'
    });
});